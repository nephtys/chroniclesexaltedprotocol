package protocol.generic

/**
  * Created by nephtys on 10/24/16.
  */
trait GenericProtocol[Aggregate] {

  trait Command extends DomainCommand[Aggregate]

  trait Event extends DomainEvent[Aggregate]

}
