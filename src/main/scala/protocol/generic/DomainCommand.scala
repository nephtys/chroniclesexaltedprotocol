package protocol.generic

import scala.util.Try


/**
  * Created by nephtys on 10/24/16.
  */
trait DomainCommand[Aggregate] {
  def validate: Try[DomainEvent[Aggregate]]
}
