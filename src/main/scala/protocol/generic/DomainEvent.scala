package protocol.generic

/**
  * Created by nephtys on 10/24/16.
  */
trait DomainEvent[Aggregate] {
  def applyEvent(x: Aggregate): Aggregate

  def ->(x: Aggregate) = applyEvent(x)
}
