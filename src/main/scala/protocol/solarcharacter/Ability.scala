package protocol.solarcharacter

/**
  * Created by nephtys on 10/24/16.
  */

object Ability {

  sealed trait Type

  case class AbilityBlock()

  //TODO: include type, rating, and specialties
  //TODO: deal with Martial Arts and Brawl specificity

  case object Unfavored extends Type {

  }

  case object Favored extends Type {

  }

  case object Caste extends Type {

  }

  case object Supernal extends Type {

  }

}

