package protocol.solarcharacter

import protocol.generic.GenericProtocol

import scala.util.{Success, Try}

/**
  * Created by nephtys on 10/24/16.
  */
object SolarBehavior extends GenericProtocol[SolarCharacterSheet] {

  object generaldata {

    case class SetNameCommand(newname: String) extends Command {
      override def validate: Try[Event] = Success(SetNameEvent(newname))
    }

    case class SetNameEvent(newname: String) extends Event {
      override def applyEvent(x: SolarCharacterSheet): SolarCharacterSheet = x.copy(name = Some(newname).filter(_.isEmpty))
    }


    //TODO: include caste

    //TODO: include willpower
  }

  object ability {

  }

  object attribute {

  }

  object merit {

  }

  object intimacy {

  }

  object aspiration {

  }

  object condition {

  }

  object charm {

  }

  object experience {

  }


}
