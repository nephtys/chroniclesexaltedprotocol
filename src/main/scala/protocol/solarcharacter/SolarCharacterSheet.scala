package protocol.solarcharacter

import scala.concurrent.Future

/**
  * Created by nephtys on 10/24/16.
  */
case class SolarCharacterSheet(
                                finishedChargen: Boolean,
                                name: Option[String],
                                player: Option[String],
                                concept: Option[String],
                                anima: Option[String],
                                caste: Option[Caste],
                                abilities: Ability.AbilityBlock,
                                attributes: Attribute.AttributePack,
                                merits: Seq[Merit.Merit],
                                intimacies: Map[Intimacy.Intimacy, Intimacy.Intensity],
                                experience: Experience.ExperienceBlock,
                                charms: Seq[Charm.Charm],
                                conditions: Seq[String],
                                aspirations: Seq[String],
                                willpower: Willpower
                              ) {


  def convertToHTMLCharacterSheet(): Future[String] = ??? //TODO: long term goal to print character sheets
}
