package protocol.solarcharacter

/**
  * Created by nephtys on 10/24/16.
  */
object Attribute {

  sealed trait Attribute {
    def name: String
  }

  sealed trait PFR extends Attribute {
    def row: Int
  }

  sealed trait PSM extends Attribute {
    def column: Int
  }

  sealed trait Physical extends PSM {
    override def column: Int = 0
  }

  sealed trait Social extends PSM {
    override def column: Int = 1
  }

  sealed trait Mental extends PSM {
    override def column: Int = 2
  }

  sealed trait Power extends PFR {
    override def row: Int = 0
  }

  sealed trait Finesse extends PFR {
    override def row: Int = 1
  }

  sealed trait Resistance extends PFR {
    override def row: Int = 2
  }

  final case class AttributePack(attributes: Map[Attribute, Int]) {
    assert(attributes.size == 9)

    def row(i: Int) = {
      assert(i >= 0 && i <= 2)
      val rows = attributes.toSeq.zipWithIndex.filter(_._2 % 3 == i)
      assert(rows.length == 3)
      rows
    }

    def column(i: Int) = {
      assert(i >= 0 && i <= 2)
      val columns = attributes.toSeq.zipWithIndex.filter(_._2 / 3 == i)
      assert(columns.length == 3)
      columns
    }
  }

  case object Strength extends Power with Physical {
    override def name: String = toString
  }

  case object Dexterity extends Finesse with Physical {
    override def name: String = toString
  }

  case object Stamina extends Resistance with Physical {
    override def name: String = toString
  }

  case object Charisma extends Power with Social {
    override def name: String = toString
  }

  case object Manipulation extends Finesse with Social {
    override def name: String = toString
  }

  case object Appearance extends Resistance with Social {
    override def name: String = toString
  }

  case object Perception extends Power with Mental {
    override def name: String = toString
  }

  case object Intelligence extends Finesse with Mental {
    override def name: String = toString
  }

  case object Wits extends Resistance with Mental {
    override def name: String = toString
  }


}
