package protocol.solarcharacter

/**
  * Created by nephtys on 10/24/16.
  */
object Experience {

  case class ExperienceBlock(spent: EXPSpent, gained: BeatsGained, ratio: BeatsEXPRatio)

  case class EXPSpent(amount: Int) extends AnyVal

  case class BeatsGained(amount: Int) extends AnyVal

  case class BeatsEXPRatio(amount: Int) extends AnyVal {
    def calculateXP(beats: BeatsGained): Int = beats.amount / amount
  }

}
