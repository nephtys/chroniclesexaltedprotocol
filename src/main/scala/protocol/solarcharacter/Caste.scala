package protocol.solarcharacter

/**
  * Created by nephtys on 10/24/16.
  */
sealed trait Caste {
  def name: String
}

case object Dawn extends Caste {
  override def name: String = "Dawn"
}

case object Zenith extends Caste {
  override def name: String = "Zenith"
}

case object Twilight extends Caste {
  override def name: String = "Twilight"
}

case object Night extends Caste {
  override def name: String = "Night"
}

case object Eclipse extends Caste {
  override def name: String = "Eclipse"
}