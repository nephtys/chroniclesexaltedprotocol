package protocol.solarcharacter

/**
  * Created by nephtys on 10/24/16.
  */
object Merit {

  case class Merit(name: String, rating: Rating)

  case class Rating(rating: Byte) extends AnyVal

}
