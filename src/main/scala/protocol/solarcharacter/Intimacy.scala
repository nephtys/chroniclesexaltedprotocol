package protocol.solarcharacter

/**
  * Created by nephtys on 10/24/16.
  */
object Intimacy {

  sealed trait Intensity extends Ordered[Intensity] {
    def weight: Int

    override def compare(that: Intensity): Int = weight.compareTo(that.weight)
  }

  sealed case class Intimacy(name: String)

  case object Minor extends Intensity {
    override def weight: Int = 1

  }

  case object Major extends Intensity {
    override def weight: Int = 2
  }

  case object Defininig extends Intensity {
    override def weight: Int = 4
  }

}
